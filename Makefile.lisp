(require 'cl-vendor)
(require 'hypo-forthern)

(vendor:eval-when-all
	(vendor:generate-exe (format nil "hypo-forthern-~a.exe"
				     (vendor::get-implementation-name))
			     #'hypo-forthern::main)
)
