(in-package hypo-forthern)

(require 'lucifer-lukism)

(lancer:enable-lancer-syntax)

(defconstant +psp-size+ #x100)
(defconstant +stack-size+ #x100)

(defun register-extended-assembler-words ()
  (declare (special *s-stack*))
  (declare (special *r-stack*))
  (declare (special *system-dict*))
  (declare (special *user-dict*))
  (declare (special *vmemory*))

  (declare (special *input-stream*))
  
  (register-system-word
   "asm<;"
   (worddef ()
	    (let* ((addr ([ *s-stack* ][ pop ]))
		   (code-list
		    (loop with code-next
		       do (setf code-next (read))
		       until (and (symbolp code-next)
				  (equal (symbol-name code-next)
					 (symbol-name 'asm>)))
		       collecting code-next)))
	      ([ (vmemory-data *vmemory*)][ memcpy
		 (lukism:lukism-generate-code-seg 'x86 code-list) addr ]))))
  (register-system-word
   "disassemble-word"
   (worddef ()
	    (let* ((name (read-word *input-stream* nil))
		   (xt (if name (word-xt name)))
		   (fn (gethash xt *xt-pool*))
		   (a ([ *s-stack* ][ pop ])))
	      (when (or (null name) (null fn))
		(format t "~&ERROR: Word name not specified.~&")
		(throw 'quit nil))
	      (let ((xt-list (funcall fn t)))
		(when (atom xt-list)
		  (format t "~&ERROR: This is not a colon word.~&")
		  (throw 'quit nil))
		(loop for n below (length xt-list)
		   do ([ *vmemory* ][ write-cell (+ a (* n +cell-size+))
			 (nth n xt-list) ]))
		([ *s-stack* ][ push a (length xt-list) ])))))
  (register-system-word
   "xt-list-to-machine"
   (worddef ()
	    (let* ((xt-assembler-addr ([ *s-stack* ][ pop ]))
		   (dest-addr ([ *s-stack* ][ pop ]))
		   (src-length ([ *s-stack* ][ pop ]))
		   (src-addr ([ *s-stack* ][ pop ])))
	      ([ *s-stack* ][ push dest-addr ])
	      (loop for n below src-length
		 do (let* ((src-xt ([ *vmemory* ][ read-cell
				      (+ src-addr (* n +cell-size+)) ]))
			   (xt-assembler 
			    ([ *vmemory* ][ read-cell
			       (+ xt-assembler-addr (* src-xt +cell-size+)) ])))
		      (when (zerop xt-assembler)
			(format t "~&ERROR: no assembler has setup for word(xt=#~a).~&"
				src-xt)
			(throw 'quit nil))
;;		      (format t "~&Assembling xt=~a(asm-xt=~a)~%" src-xt xt-assembler)
		      (catch 'next-word
			(if (zerop src-xt)
			    (let ((operand ([ *vmemory* ][ read-cell
					      (+ src-addr (* (1+ n) +cell-size+)) ])))
			      ([ *s-stack* ][ push operand ])
			      (incf n)
			      (execute-xt xt-assembler)
			      )
			    (execute-xt xt-assembler))))))))
  
  (register-system-word
   "dump-mem!"
   (worddef ()
	    (dump-mem)))
  
  (register-system-word
   "clean-exit!"
   (worddef ()
	    (delete-file "forthern.com")
	    (throw 'exit-system-no-save-mem nil)
	    )
   )
  )

(defun dump-mem ()
  (declare (special *vmemory*))
  (format t "~&Dumping memory to file...")  
  (with-open-file (s "forthern.com" :direction :output
		     :if-exists :supersede :element-type 'unsigned-byte)
    (bitblock:bitblock-dump
     (vmemory-data *vmemory*) s +psp-size+
     (- +memory-bytesize+ +psp-size+ +stack-size+)))
  (format t "Done~&"))

(defun load-memory ()
  (declare (special *vmemory*))
  (with-open-file (*standard-input* "forthern.com" :direction :input
				    :if-does-not-exist nil :element-type 'unsigned-byte)
    (when *standard-input*
      (format t "~&Loading memory from file...~%")
      (loop with counter = +psp-size+ and cur-byte = nil
	 and end-pos = (- +memory-bytesize+ +psp-size+ +stack-size+)
	 while (and (setf cur-byte (read-byte *standard-input* nil nil))
		    (< counter end-pos))
	 do (progn ([ *vmemory* ][ write-byte counter cur-byte ])
		   (incf counter)
		   (when (zerop (mod counter 1024))
		     (format t "."))))
      (fresh-line)
      t)))

(eval-when (:load-toplevel :execute)
  (register-extended-word-hook #'register-extended-assembler-words))


(lancer:disable-lancer-syntax)
