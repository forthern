(in-package hypo-forthern)

(defun register-extended-constant-words ()
  (declare (special *s-stack*))
  (declare (special *r-stack*))
  (declare (special *system-dict*))
  (declare (special *user-dict*))
  (declare (special *vmemory*))

  (declare (special *dp*))
  (declare (special *base-ptr*))
  
  (register-system-word
   "dp"
   (worddef ()
     (push-number *dp*)))

  (register-system-word
   "base"
   (worddef ()
     (push-number *base-ptr*)))

  
  (register-system-colon-word
   "here" "dp @")

  (register-system-colon-word
   "?" "@ .")
  
  )

(eval-when (:load-toplevel :execute)
  (register-extended-word-hook #'register-extended-constant-words))

