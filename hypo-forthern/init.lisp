
(in-package hypo-forthern)

(lancer:enable-lancer-syntax)

(defvar *system-dict* (make-instance 'dictionary))
(defvar *user-dict* (make-instance 'dictionary))
(defvar *s-stack* (make-instance 'stack))
(defvar *r-stack* (make-instance 'stack))
(defvar *vmemory*    (make-instance 'vmemory))


(defvar *input-line* nil)
(defvar *input-stream* nil)

(defvar *base-ptr* #x500)
(defvar *dp* #x9F0)
(defconstant +initial-here+ #x1000)
(defconstant +no-load-init-file-flag-addr+ #xDEAD)

(defun execute-file (filename)
  (with-open-file (*standard-input*  filename :direction :input
				     :if-does-not-exist nil)
    (when *standard-input*
      (loop
	 do (setf *input-line* (read-line *standard-input* nil nil))
	 while *input-line*
	 do (execute-line nil)))))


(defun-exported system-init ()
  (setf (dictionary-parent *user-dict*) *system-dict*)
  (setf (dictionary-system *system-dict*) t)
  (register-system-words)

  (clear-rubbish)
  
  (when (not (load-memory))
    ([ *vmemory* ][ write-cell *dp* +initial-here+ ])
    )

  (if (/= ([ *vmemory* ][ read-cell +no-load-init-file-flag-addr+ ]) #x4D81)
      (catch 'quit
	(execute-file ".hypoforthern.fs" )))
  )

(defun-exported showlogo ()
  (format t "~3&  Welcome to cl-forthern[hypo] 0.01~&     by CrLF0710 ~2&"))

(defun clear-rubbish ()
  (setf (stack-data *s-stack*) nil)
  (setf (stack-data *r-stack*) nil)
  ([ *vmemory* ][ write-cell *base-ptr* 10 ])
  )

(defun word-xt (word) ([ *user-dict* ][ @ word nil ]))
(defun execute-word (word)
;;  (format t "DEBUG: ~a" word)
  (let ((xt (word-xt word)))
    (if xt
	(progn (execute-xt xt)
	       (throw 'next-word nil)))))

(defun word-is-number (word)
  (handler-bind ((error (lambda (condition)
			  (format t "~&ERROR: ~a is undefined" word)
			  (throw 'quit nil))))
    (parse-integer (symbol-name word) :radix ([ *vmemory* ][ read-cell *base-ptr* ]))))

(defun read-nonblank (stream eof-sym)
  (labels ((symbol-char-p (c)
	  (and (graphic-char-p c)
	       (not (equal c #\Space)))))
    (peek-char t stream nil nil)
    (let* ((next-char)
	   (next-word
	    (loop while (progn (setf next-char
				     (peek-char nil stream nil nil))
			       (and next-char
				    (symbol-char-p next-char)))
	       collecting (string (read-char stream nil nil)))))
      (if next-word
	  (apply #'concatenate (cons 'string next-word))
	  eof-sym))))

(defun any-to-key (x)
  (intern (format nil "~(~a~)" x)))
				       
(defun read-word (stream eof-sym)
  (declare (special *input-stream*))
  (let ((val (read-nonblank stream eof-sym)))
    (if (not (eql val eof-sym))
	(setf val (any-to-key val)))
    val
  ))

(defun execute-line (&optional (use-ok t))
  (when *input-line*
    (with-input-from-string (*input-stream* *input-line*)
      (catch 'finished-processing
	(loop
	   (catch 'next-word
	     (let* ((eof-sym (gensym))
		    (word (read-word *input-stream* eof-sym)))
	       (if (eq word eof-sym) (throw 'finished-processing nil))
	       (execute-word word)
	       (push-number (word-is-number word))
	       ))))
      (if use-ok
	  (format t " ok~&")))))

(defun-exported repl ()
    (labels ((prompt () (format t "~&> ")(force-output)))
      (loop
	 (progn
	   (prompt)
	   (let ((*input-line* (read-line *standard-input* nil nil nil)))
	     (execute-line))))))

(defun-exported main (&rest args)
  (declare (ignore args))
  (system-init)

  (catch 'exit-system-no-save-mem
    (catch 'exit-system
      (showlogo)
      (loop
	 (catch 'quit
	   (clear-rubbish)
	   (repl)
	   )))
    (dump-mem)
    )

  #+ccl
  (ccl:quit)
  #+sbcl
  (sb-ext:quit)
  #+allegro
  (excl:exit)
  )

(eval-when (:load-toplevel :execute)
  (import 'main (find-package 'cl-user)))

(lancer:disable-lancer-syntax)
