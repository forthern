(require 'lucifer-lancer)

(in-package hypo-forthern)

(defclass dictionary ()
  ((data :initform (make-hash-table :test 'equal)
	 :accessor dictionary-data)
   (system :initform nil
	   :accessor dictionary-system)
   (parent :initform nil
	   :accessor dictionary-parent)))

(defun symbol-to-key (symbol)
  (string-upcase (symbol-name symbol)))

(define-lancer-method (dictionary @)
    dictionary-read-value ((dict dictionary) symbol &optional (error-p nil))
    (let* ((default (gensym))
	   (val (gethash (symbol-to-key symbol)
			 (dictionary-data dict) default)))
      (if (eq val default)
	  (or (and (dictionary-parent dict)
		   (dictionary-read-value (dictionary-parent dict)
					  symbol error-p))
	      (if error-p (if error-p (assert (not (eq val default))))))
	  val)))

(define-lancer-method (dictionary erase)
    dictionary-erase ((dict dictionary) symbol)
    (remhash (symbol-to-key symbol) (dictionary-data dict)))

(define-lancer-method (dictionary !)
    dictionary-write-value ((dict dictionary) symbol value)
    (setf (gethash (symbol-to-key symbol) (dictionary-data dict))
	  value))

(define-lancer-method (dictionary find)
    dictionary-find ((dict dictionary) value)
    (loop for item-value being each hash-value in (dictionary-data dict)
       using (hash-key item-key)
       do (if (eql item-value value) (return-from dictionary-find item-key))))
