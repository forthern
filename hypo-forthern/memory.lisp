(require 'lucifer-lamack)

(in-package hypo-forthern)

(lancer:enable-lancer-syntax)

(defconstant +byte-bitsize+ 8)
(defconstant +cell-bitsize+ 32)
(defconstant +cell-size+ (/ +cell-bitsize+ +byte-bitsize+))
(defconstant +memory-bytesize+ (* 1024 64))

(defclass vmemory ()
  ((data :initform (bitblock::bitblock-varsized +memory-bytesize+
						+byte-bitsize+)
		   :accessor vmemory-data)
   ))

(define-lancer-method (vmemory write-byte)
    vmemory-write-byte ((vmemory vmemory) addr value)
    ([ (vmemory-data vmemory) ][ write-byte addr value ])
    )

(define-lancer-method (vmemory read-byte)
    vmemory-read-byte ((vmemory vmemory) addr)
    ([ (vmemory-data vmemory) ][ read-byte addr ])
    )

(define-lancer-method (vmemory write-cell)
    vmemory-write-cell ((vmemory vmemory) addr value)
    ([ (vmemory-data vmemory) ][ assign-fixnum
       (bittype:int-to-uint value +cell-bitsize+)
       nil addr +cell-size+ ])
    )

(define-lancer-method (vmemory read-cell)
    vmemory-read-cell ((vmemory vmemory) addr)
    (bittype:uint-to-int
     ([ (vmemory-data vmemory) ][ read-fixnum
	nil addr +cell-size+ ]) +cell-bitsize+)
    )

(lancer:disable-lancer-syntax)

