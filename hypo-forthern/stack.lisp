(require 'lucifer-lancer)
(require 'lucifer-lamack)

(in-package hypo-forthern)

(defclass stack ()
  ((data :initform nil :accessor stack-data)
   ))

(defconstant +stack-elemt-bitcount+ 32)

(defun to-u (a)
  (bittype:int-to-uint a +stack-elemt-bitcount+))
(defun to-ud (a)
  (bittype:int-to-uint a (* 2 +stack-elemt-bitcount+)))

(defun floor-/ ( a b)
  (floor (/ a b)))

(define-lancer-method (stack push)
    stack-push ((stack stack) &rest values)
    (loop for value in values
       do (push (bittype:uint-to-int
		 (bittype:int-to-uint value +stack-elemt-bitcount+)
		 +stack-elemt-bitcount+) ;;overflow-simulate
		(stack-data stack)))
    )

(define-lancer-method (stack push2)
    stack-push2 ((stack stack) &rest values)
    (loop for value in values
       do (stack-push stack value (ash value (- +stack-elemt-bitcount+)))))
			     
(define-lancer-method (stack pop)
    stack-pop ((stack stack))
;;    (assert (stack-data stack))
    (if (null (stack-data stack))
	(progn (format t "~&STACK OVERFLOW!~&")
	       (throw 'quit nil)))
      (pop (stack-data stack)))

(define-lancer-method (stack pop2)
    stack-pop2 ((stack stack))
    (let* ((h (stack-pop stack))
	   (l (stack-pop stack)))
      (bittype:uint-to-int (logior (ash (to-u h) +stack-elemt-bitcount+) (to-u l))
			   (* 2 +stack-elemt-bitcount+))))

(define-lancer-method (stack peek)
    stack-peek ((stack stack))
    ;;    (assert (stack-data stack))
    (if (null (stack-data stack))
	(progn (format t "~&STACK OVERFLOW!~&")
	       (throw 'quit nil)))
    (first (stack-data stack)))

(define-lancer-method (stack pick)
    stack-pick ((stack stack) index)
    ;;    (assert (stack-data stack))
    (if (>= index (length (stack-data stack)))
	(progn (format t "~&STACK OVERFLOW!~&")
	       (throw 'quit nil)))
    (nth index (stack-data stack)))
