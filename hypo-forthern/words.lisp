(require 'lucifer-luciffi)
(in-package hypo-forthern)

(lancer:enable-lancer-syntax)

(luciffi:defcfun ("_getch" getch) :int)
(luciffi:defcfun ("_kbhit" kbhit) :boolean)

(defvar *xt-pool* (make-hash-table :test 'eq))
(defvar *xt-counter* 0)
(defconstant +true-const+ -1)
(defconstant +false-const+ 0)
(defvar *compiled-mode* nil)

(defvar *branch-word-xt-list* nil)
(defvar *branch-state-list* nil)

(defun push-number (val)
  (declare (special *s-stack*))
  ([ *s-stack* ][ push val ])
  (throw 'next-word nil))

(defun bool-to-int (b)
  (if b
      +true-const+
      +false-const+))


(defun on-branch-p ()
  (or (null *branch-state-list*)
      (first *branch-state-list*)))

(defun is-branch-word (xt)
  (find xt *branch-word-xt-list*))

(defun register-xt (fn)
  (incf *xt-counter*)
  (setf (gethash *xt-counter* *xt-pool*) fn)
  *xt-counter*
)

(defun xt-word (xt)
  (declare (special *system-dict*))
  (declare (special *user-dict*))
  (or ([ *system-dict* ][ find xt ])
      ([ *user-dict*   ][ find xt ])))


(defun execute-xt (xt)
  (let ((fn (gethash xt *xt-pool*)))
    (if fn
	(progn
	  (funcall fn)
	  (throw 'next-word nil))
	(progn
	  (format t " ERROR: invalid function!~&")
	  (throw 'quit nil)))))

(defvar *cur-xt-index* nil)

(defun execute-xt-list (xt-list)
  (let ((*compiled-mode* t))
    (catch 'exit
      (loop with escape-on = nil
	 for xt-index below (length xt-list)
	 do (let ((xt (nth xt-index xt-list)))
	      (setf *cur-xt-index* xt-index)
	      (catch 'next-word
		(cond
		  (escape-on (progn (setf escape-on nil)
				    (when (on-branch-p)
				      (push-number xt))))
		  ((eq xt 0) (progn (setf escape-on t)
				    (throw 'next-word nil)))
		  (t         (progn (when (or (on-branch-p)
					      (is-branch-word xt))
				      (execute-xt xt))
				    (throw 'next-word nil)))))
	      (setf xt-index *cur-xt-index*))))))

(defun curpos-in-xt-list ()
  *cur-xt-index*)

(defun jump-to-pos-in-xt-list (pos)
  (setf *cur-xt-index* pos))

(defun see-disassemble (xt-list)
  (if (not (listp xt-list)) xt-list
      (apply #'concatenate
	     (cons 'string 
		   (loop with escape-on = nil
		      for xt in xt-list
		      if escape-on  collecting
			(progn
			  (setf escape-on nil)
			  (format nil "~a " xt))
		      else collecting
			(if (= xt 0)
			    (progn (setf escape-on t)
				   "Lit ")
			    (or (format nil "~a " (xt-word xt))
				(format nil "<Unnamed-Word #~a> " xt))))))))

(defun register-system-xt (word xt)
  (declare (special *system-dict*))
  (let ((key (intern (format nil "~(~a~)" word))))
    ([ *system-dict* ][ ! key xt ])
    ))
    
(defun register-system-word (word fn &optional branch-p)
  (let ((xt (register-xt fn)))
    (register-system-xt word xt)
    (when branch-p
      (pushnew xt *branch-word-xt-list*))
  ))

(defun register-system-colon-word (word text)
  (register-system-word word (make-colon-word-closure (colon-compile-text text))))

(defun colon-compile-input-to-xt-list (stream)
  (let* ((eof-sym (gensym))
	 (semicolon-sym (intern ";")))
    (loop with word
	until (or (eq (setf word (read-word stream eof-sym)) eof-sym)
		  (eq word semicolon-sym))
	appending
	  (let ((xt (word-xt word)))
	    (if (null xt)
		(progn ; lit 
		  (let ((xval (word-is-number word)))
		    (list 0 xval)))
		(list xt))))))

(defun colon-compile-text (text)
  (declare (special *input-stream*))
  (with-input-from-string (*input-stream* (concatenate 'string text " ;"))
    (assert *input-stream*)
    (colon-compile-input-to-xt-list *input-stream*)))


(defun make-colon-word-closure (xt-list)
  (lambda (&optional (see))
    (if see
	xt-list
	(execute-xt-list xt-list))))

(defun make-create-word-closure (val)
  (lambda (&optional (see))
    (if see
	(list 0 val)
	(push-number val))))

(defmacro worddef (params &body body)
  `(lambda (,@params &optional see)
     (if see
	 "#<Native code>"
	 (progn ,@body))))


(defparameter *extended-word-hooks* nil)

(defun register-system-words ()
  (declare (special *input-stream*))
  (declare (special *dp*))
  
  (declare (special *s-stack*))
  (declare (special *r-stack*))
  (declare (special *system-dict*))
  (declare (special *user-dict*))
  (declare (special *vmemory*))

  (register-system-word
   "bye" (worddef () (throw 'exit-system nil)))
  (register-system-word
   "quit" (worddef () (throw 'quit nil)))
  (register-system-word
   "."   (worddef () (format t " ~d" ([ *s-stack* ][ pop ]))))
  (register-system-word
   "u."   (worddef () (format t " ~d" (to-u ([ *s-stack* ][ pop ])))))
  (register-system-word
   "d."   (worddef () (format t " ~d" ([ *s-stack* ][ pop2 ]))))
  (register-system-word
   "ud."   (worddef () (format t " ~d" (to-ud ([ *s-stack* ][ pop2 ])))))
  (register-system-word
   ".s"  (worddef () (let ((stack-data (stack-data *s-stack*)))
		       (format t " [~d] ~{~a~^ ~}" (length stack-data)
			       (reverse stack-data)))))

  (register-system-word  ;;toy
   ":"   (worddef ()
		  (let* ((eof-sym (gensym))
			 (semicolon-sym (intern ";"))
			 (name (read-word *input-stream* eof-sym))
			 (xt-list (colon-compile-input-to-xt-list *input-stream*)))
		    (when (null name)
		      (format t "~&ERROR: COLON-word name not specified.~&")
		      (throw 'finished-processing nil))
		    ([ *user-dict* ][ ! (any-to-key name)
		       (register-xt (make-colon-word-closure xt-list)) ])
		    )))
  (register-system-word
   ";"   (worddef ()
		  (format t "~&ERROR: SEMICOLON is only used in compilation mode")
		  (throw 'finished-processing nil)))
  
  ;;core-words & some extended words
  (register-system-word
   "!"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			    (a ([ *s-stack* ][ pop ])))
		       ([ *vmemory* ][ write-cell b a ]) )))

  (register-system-word
   "+"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			    (a ([ *s-stack* ][ pop ])))
		       ([ *s-stack* ][ push (+ a b) ]))))
  (register-system-word
   "+!"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			     (a ([ *s-stack* ][ pop ])))
			([ *vmemory* ][ write-cell b
			   (+ ([ *vmemory* ][ read-cell b ]) a) ]) )))

  (register-system-word
   "-"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			    (a ([ *s-stack* ][ pop ])))
		       ([ *s-stack* ][ push (- a b) ]))))
  (register-system-word
   "<"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			    (a ([ *s-stack* ][ pop ])))
		       ([ *s-stack* ][ push (bool-to-int (< a b)) ]) )))
  (register-system-word
   "<>"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			     (a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (bool-to-int (/= a b)) ]) )))
  (register-system-word
   "="   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			    (a ([ *s-stack* ][ pop ])))
		       ([ *s-stack* ][ push (bool-to-int (= a b)) ]) )))
  (register-system-word
   ">"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			    (a ([ *s-stack* ][ pop ])))
		       ([ *s-stack* ][ push (bool-to-int (> a b)) ]) )))
  (register-system-word
   ">r"   (worddef () ([ *r-stack* ][ push ([ *s-stack* ][ pop ]) ])))
  (register-system-word
   "?dup"   (worddef () (let* ((a ([ *s-stack* ][ peek ])))
			  (if (/= a 0)
			      ([ *s-stack* ][ push a ]) ))))
  (register-system-word
   "@"   (worddef () (let* ((a ([ *s-stack* ][ pop ])))
		       ([ *s-stack* ][ push
			  ([ *vmemory* ][ read-cell a ]) ]))))
  (register-system-word
   "0<"   (worddef () (let* ((a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (bool-to-int (< a 0)) ]) )))
  (register-system-word
   "0="   (worddef () (let* ((a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (bool-to-int (= a 0)) ]) )))
  (register-system-word
   "1+"   (worddef () (let* ((a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (1+ a) ]) )))
  (register-system-word
   "1-"   (worddef () (let* ((a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (1- a) ]) )))
  (register-system-word
   "2*"   (worddef () (let* ((a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (ash a 1) ]) )))
  (register-system-word
   "2/"   (worddef () (let* ((a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (ash a -1) ]) )))

  (register-system-word
   "abs"   (worddef () (let ((a ([ *s-stack* ][ pop ])))
			 ([ *s-stack* ][ push (abs a) ]))))

  (register-system-word
   "and"  (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			     (a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (logand a b) ]))))
  (register-system-word
   "begin"   (worddef () (if *compiled-mode*
                             (progn
                                (when (on-branch-p)
                                        ([ *r-stack* ][ push (curpos-in-xt-list) ])
                                )
                                (push (on-branch-p) *branch-state-list*)
                             )
			     (format t "~&ERROR: BEGIN is compilation only.~&"))) t)
  (register-system-word
   "constant"  (worddef ()
			(let* ((name (read-word *input-stream* nil))
			       (a ([ *s-stack* ][ pop ])))
			  (when (null name)
			    (format t "~&ERROR: CREATE-word name not specified.~&")
			    (throw 'finished-processing nil))
			  ([ *user-dict* ][ ! (any-to-key name)
			     (register-xt (make-create-word-closure a)) ]))))

  (register-system-word
   "c!"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			     (a ([ *s-stack* ][ pop ])))
			([ *vmemory* ][ write-byte b a ]))))
  (register-system-word
   "c@"   (worddef () (let* ((a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push
			   ([ *vmemory* ][ read-byte a ]) ]))))

  (register-system-word
   "do"   (worddef () (if *compiled-mode*
			  (progn
			    (when (on-branch-p)
			      (let* ((b ([ *s-stack* ][ pop ]))
				     (a ([ *s-stack* ][ pop ])))
				([ *r-stack* ][ push a b (curpos-in-xt-list) ])))
			    (push (on-branch-p) *branch-state-list*))
			  (format t "~&ERROR: DO is compilation only.~&"))))
  (register-system-word
   "drop"   (worddef () ([ *s-stack* ][ pop ])))

  (register-system-word
   "dup"   (worddef () (let ((a ([ *s-stack* ][ pop ])))
			 ([ *s-stack* ][ push a a ]))))
  
  (register-system-word
   "else"   (worddef () (if *compiled-mode*
			    (push  (not (pop *branch-state-list*)) *branch-state-list*)
			    (format t "~&ERROR: ELSE is compilation only.~&"))) t)

  (register-system-word
   "emit"   (worddef () (format t "~a" (code-char ([ *s-stack* ][ pop ])))))

  (register-system-word
   "execute"  (worddef () (execute-xt ([ *s-stack* ][ pop ]))))

  (register-system-word
   "exit"   (worddef () (if *compiled-mode*
			    (throw 'exit nil)
			    (format t "~&ERROR: EXIT is compilation only.~&"))))
  
  (register-system-word
   "fill"   (worddef () (let* ((c ([ *s-stack* ][ pop ]))
			       (b ([ *s-stack* ][ pop ]))
			       (a ([ *s-stack* ][ pop ])))
			  (loop for n below (to-u b) do
			       ([ *vmemory* ][ write-byte (+ a n) c ])))))
  
  (register-system-word
   "i"   (worddef () (let* ((a ([ *r-stack* ][ pick 1 ])))
		       ([ *s-stack* ][ push a ]))))

  (register-system-word
   "if"   (worddef () (if *compiled-mode*
			  (let ((a ([ *s-stack* ][ pop ])))
			    (push  (and (on-branch-p)
					(not (zerop a))) *branch-state-list*))
			  (format t "~&ERROR: IF is compilation only.~&"))) t)

  (register-system-word
   "invert"   (worddef () (let ((a ([ *s-stack* ][ pop ])))
			    ([ *s-stack* ][ push (lognot a) ]))))

  (register-system-word
   "j"   (worddef () (let* ((a ([ *r-stack* ][ pick 4 ])))
		       ([ *s-stack* ][ push a ]))))

  (register-system-word
   "k"   (worddef () (let* ((a ([ *r-stack* ][ pick 7 ])))
		       ([ *s-stack* ][ push a ]))))

  (register-system-word
   "key"   (worddef () ([ *s-stack* ][ push (getch) ])))

  (register-system-word
   "key?"   (worddef () ([ *s-stack* ][ push (bool-to-int (kbhit)) ])))

  (register-system-word
   "leave"  (worddef () (if *compiled-mode*
			    (progn
			      (pop *branch-state-list*)
			      (push nil *branch-state-list*))
			    (format t "~&ERROR: LEAVE is compilation only.~&"))))
  (register-system-word
   "leave2"  (worddef () (if *compiled-mode*
			    (progn
			      (pop *branch-state-list*) (pop *branch-state-list*)
			      (push nil *branch-state-list*) (push nil *branch-state-list*))
			    (format t "~&ERROR: LEAVE2 is compilation only.~&"))))
  (register-system-word
   "loop"   (worddef () (if *compiled-mode*
			    (progn
			      (when (on-branch-p)
				(let* ((c ([ *r-stack* ][ pop ]))
				       (b ([ *r-stack* ][ pop ]))
				       (a ([ *r-stack* ][ pop ])))
				  (incf b)
				  (when (< b a)
				    ([ *r-stack* ][ push a b c ])
				    (jump-to-pos-in-xt-list c)
				    ))
				)
			      (pop *branch-state-list*)
			      )
			    (format t "~&ERROR: LOOP is compilation only.~&"))))
  
  (register-system-word
   "lshift"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
				 (a ([ *s-stack* ][ pop ])))
			    ([ *s-stack* ][ push (ash (to-u a) (to-u b)) ]))))

  (register-system-word
   "m+"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			     (a ([ *s-stack* ][ pop2 ])))
			([ *s-stack* ][ push2 (+ a b) ]))))
  
  (register-system-word
   "nip"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			      (a ([ *s-stack* ][ pop ])))
			 ([ *s-stack* ][ push b ]))))
  
  (register-system-word
   "negate"   (worddef () (let ((a ([ *s-stack* ][ pop ])))
			    ([ *s-stack* ][ push (- a) ]))))
  
  (register-system-word
   "or"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			     (a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (logior a b) ]))))

  (register-system-word
   "over"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			       (a ([ *s-stack* ][ pop ])))
			  ([ *s-stack* ][ push a b a ]))))

  (register-system-word
   "pick"   (worddef () (let ((a ([ *s-stack* ][ pop ])))
			  ([ *s-stack* ][ push ([ *s-stack* ][ pick a ]) ]))))
  (register-system-word
   "repeat"   (worddef () (if *compiled-mode*
			     (progn
			       (if (on-branch-p)
				   (let* ((a ([ *r-stack* ][ pop ])))
				     ([ *r-stack* ][ push a ])
				     (jump-to-pos-in-xt-list a)))
			       (pop *branch-state-list*))
			     (format t "~&ERROR: UNTIL is compilation only.~&"))) t)  
  (register-system-word
   "rot"   (worddef () (let* ((c ([ *s-stack* ][ pop ]))
			      (b ([ *s-stack* ][ pop ]))
			      (a ([ *s-stack* ][ pop ])))
			 ([ *s-stack* ][ push b c a ]))))
  
  (register-system-word
   "rshift"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
				 (a ([ *s-stack* ][ pop ])))
			    ([ *s-stack* ][ push (ash (to-u a) (- (to-u b))) ]))))
  
  (register-system-word
   "r>"   (worddef () ([ *s-stack* ][ push ([ *r-stack* ][ pop ]) ])))
  
  (register-system-word
   "r@"   (worddef () ([ *s-stack* ][ push ([ *r-stack* ][ peek ]) ])))
  
  (register-system-word
   "swap"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			       (a ([ *s-stack* ][ pop ])))
			  ([ *s-stack* ][ push b a ]))))

  (register-system-word
   "then"   (worddef () (if *compiled-mode*
			    (pop *branch-state-list*)
			    (format t "~&ERROR: THEN is compilation only.~&"))) t)
  (register-system-word
   "tuck"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			       (a ([ *s-stack* ][ pop ])))
			  ([ *s-stack* ][ push b a b ]))))
  (register-system-word
   "um*"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			      (a ([ *s-stack* ][ pop ])))
			 ([ *s-stack* ][ push2 (* (to-u a) (to-u b)) ]) )))
  (register-system-word
   "um/mod" (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			       (a ([ *s-stack* ][ pop2 ])))
			  ([ *s-stack* ][ push
			     (mod (to-ud a) (to-u b))			    
			     (floor-/ (to-ud a) (to-u b))
			     ]) )))
  (register-system-word
   "until"   (worddef () (if *compiled-mode*
			     (progn
			       (if (on-branch-p)
				   (let* ((b ([ *s-stack* ][ pop ]))
					  (a ([ *r-stack* ][ pop ])))
				     (if (zerop b)
                                       (progn
                                          ([ *r-stack* ][ push a ])
				          (jump-to-pos-in-xt-list a)))))
			       (pop *branch-state-list*))
			     (format t "~&ERROR: UNTIL is compilation only.~&"))) t)
  (register-system-word
   "u<"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			     (a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (bool-to-int (< (to-u a) (to-u b))) ]) )))

  (register-system-word
   "u>"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			     (a ([ *s-stack* ][ pop ])))
			([ *s-stack* ][ push (bool-to-int (> (to-u a) (to-u b))) ]) )))

  (register-system-word
   "unloop"   (worddef () ([ *r-stack* ][ pop ])
		       ([ *r-stack* ][ pop ])
		       ([ *r-stack* ][ pop ])))
  
  (register-system-word
   "variable"  (worddef ()
			(let* ((name (read-word *input-stream* nil))
			       (here ([ *vmemory* ][ read-cell *dp* ])))
			  (when (null name)
			    (format t "~&ERROR: CREATE-word name not specified.~&")
			    (throw 'finished-processing nil))
			  ([ *user-dict* ][ ! (any-to-key name)
			     (register-xt (make-create-word-closure here)) ])
			  ([ *vmemory* ][ write-cell *dp* (+ here +cell-size+) ]))))
  (register-system-word
   "while"    (worddef () (if *compiled-mode*
			      (progn
				(when (on-branch-p)
				    (when (zerop ([ *s-stack* ][ pop ]))
				      (pop *branch-state-list*)
				      (push nil *branch-state-list*))))
			      (format t "~&ERROR: WHILE is compilation only.~&"))))
  
  (register-system-word
   "xor"   (worddef () (let* ((b ([ *s-stack* ][ pop ]))
			      (a ([ *s-stack* ][ pop ])))
			 ([ *s-stack* ][ push (logxor a b) ]))))
  
  ;;end of core words
  (register-system-word
   "'"     (worddef () ([ *s-stack* ][ push
			  (let* ((word (read-word *input-stream* nil))
				 (xt (if word (word-xt word))))
			    (if (null xt)
				(progn
				  (format t "ERROR: invalid word specified")
				  (throw 'quit nil))) xt)  ])))
  (register-system-word
   "see"  (worddef () (let* ((word (read-word *input-stream* nil))
			     (xt (if word (word-xt word)))
			     (fn (gethash xt *xt-pool*)))
			(when (null xt)
			  (format t "ERROR: invalid word specified")
			  (throw 'quit nil))
			(format t "~& : ~a~&   ~a ;~2%" word
				(see-disassemble (funcall fn t))))))
  (register-system-word
   "forget"  (worddef () (let* ((word (read-word *input-stream* nil))
				(xt (if word (word-xt word))))
			   (when (or (null xt)
				     (not ([ *user-dict* ][ erase word ])))
			     (format t "ERROR: invalid word or protected word specified")
			     (throw 'quit nil)))))

  (register-system-word
   "--"      (worddef () (loop while (read-word *input-stream* nil))) t)
  
  (register-system-word
   "words" (worddef ()
		    (labels ((dump-dictionary (dict-name dict-hashtable)
			       (format t "~&[Dictionary <~a>]:~2& ~@[~{~a~^ ~}~2&~]"
				       dict-name
				       (loop for word-symbol being each hash-key
					  in dict-hashtable
					  collect word-symbol))))
		      (dump-dictionary "system" (dictionary-data *system-dict*))
		      (dump-dictionary "user" (dictionary-data *user-dict*)))))

  ;;experimental
  (loop for hook in *extended-word-hooks*
     do (funcall hook)
       )
  )

(defun register-extended-word-hook (hook-fn)
  (pushnew hook-fn *extended-word-hooks*))


(lancer:disable-lancer-syntax)
