all:
	@echo 'Use "make sbcl" or "make ccl" instead.'

sbcl:
	make sbcl-make sbcl-rar

sbcl-make:
	sbcl --load "Makefile.lisp" || true 

sbcl-rar:
	rm -f cl-forthern.rar
	rar a cl-forthern.rar hypo-forthern-SBCL.exe readme.txt changelog.txt

ccl:
	make ccl-make ccl-rar

ccl-make:
	ccl --load 'Makefile.lisp'

ccl-rar:
	rm -f cl-forthern.rar
	rar a cl-forthern.rar hypo-forthern-CCL.exe readme.txt changelog.txt

