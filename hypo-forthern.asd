
(defpackage hypo-forthern-asd
  (:use :cl :asdf))

(in-package hypo-forthern-asd)

(defsystem hypo-forthern
  :name "Hypo.Forthern"
  :author "Charles Lew(crlf0710@gmail.com)"
  :description "A prototype Forth interpreter"
  :pathname #-asdf2 (merge-pathnames "hypo-forthern/" *load-truename*)
            #+asdf2 "hypo-forthern/"
	    :depends-on (:lucifer-lutilities :lucifer-lancer :lucifer-lamack
					     :lucifer-lukism :lucifer-luciffi)
  :components ((:file "package")
	       (:file "stack" :depends-on ("package"))
	       (:file "dictionary" :depends-on ("package"))
	       (:file "memory" :depends-on ("package"))
	       (:file "words" :depends-on ("stack" "dictionary" "memory"))
	       (:file "extended/constant" :depends-on ("words"))
	       (:file "extended/assembler" :depends-on ("words"))
	       (:file "init" :depends-on ("words"))))
